<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function show ()
    {
      $products =   Product::all();
      return $products->toJson();
    }
    public function add() {
        $product = new Product();
        $product->name = Str::random(10);
        $product->articleID = Str::random(10);
        $product->price = rand(0, 100);
        $product->width = rand(0,100);
        $product->height = rand(0,100);
        $product->description = Str::random(10);
        $product->save();
    }
}
